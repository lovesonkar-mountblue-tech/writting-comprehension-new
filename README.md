# Lucene Vs. ElasticSearch Vs. SolR

## What is Lucene?

Lucene is a Java library that is used for building indexes that are easily searchable and indexable. In simple words, it is a Java search library used to indexing data and search for data in a fast and efficient manner. Various search engines have been built based on this library.

**There are two key aspects:**

1. The way how the data has to be stored.

2. The way how we query or search for indexed data.


## Advantages

* Scalable and High-performance Indexing.

* Powerful, Accurate, and Efficient Search Algorithm.

* Open-source and purely written in Java.

* Solr is built on top of Lucene.


## Why Indexing?

![File Indexing](./fileIndexing.png)

* Indexing is used to make sure that the stored data is retrievable in the most efficient and fastest way.

* The primary reason for the indexing of data is to optimize the speed and performance for easy data retrieval.

* Without an index, the search engine would scan every document in the corpus, which would require a lot of computing power.

* For example, while an index of 10,000 documents can be queried within a few milliseconds, a sequential scan of every word in 10,000 large documents could take hours.


### Indexing Flow:

* A document in Lucene’s terminology is a “record”.

* These records have to be analyzed before submitting to the indexing process. The analyzer analyzes the document’s type and content. This process itself is known as the “analysis” process.

* Even after the “analysis” process is done, the record cannot be stored in indexes. It first has to be tokenized before getting indexed. In this process, the record is broken into smaller units called “tokens”  and each token is assigned a “Term Vector” that has three attributes i.e. position, offset, and length of the token. This process is known as “tokenization”.

* At last, the “Indexing” process is done.

![Indexing](./Indexing.png)

**We can get the better idea of indexing from the following example:**

![Indexing Example](./Indexing2.png)


## Lucene: Searching In Index

As it is mentioned above that there are two key aspects in building a search platform i.e. “Indexing” and “Searching”, so once after indexing the data there are multiple ways of retrieving the data.

1. Lucene APIs

2. Query Parser (like in SQL, PSQL, etc.)


**Query Parser translates a textual expression from the end into an arbitrarily complex query for searching**

![Searching In Indexing](./searchingIndexing.png)



# SearchEngines: Solr and Elasticsearch

* Solr and Elasticsearch are complete open-source search engines that are built based on a Java-based search library named Lucene. They are capable to offer search results in a complete user-friendly manner.

* Solr and ElasticSearch are competing search engines. As both ElasticSearch and Solr are built on top of Lucene, so many of the functionalities provided by them are almost the same.

* Solr and ElasticSearch take Lucene APIs, add features on top of them, and make the APIs accessible through an easy to deploy webserver (like tomcat, etc.).


## ElasticSearch vs. Solr

![elasticSolr](./elasticSolr.jpeg)

* Solr can accept data from multiple sources that include CSV files, XML files, and also data extracted directly from the tables and file formats like MS Word and PDF. On the other hand, Elasticsearch can accept data from ActiveMQ, DynamoDB (Amazon NoSQL), AWS SQS, File System, Git, JMS, Kafka, JDBC, LDAP, MongoDB, neo4j, Redis, Solr, RabbitMQ, and Twitter.

* Solr is now available with a good range of Rest APIs turning it into a better choice than Elasticsearch while on the other hand, Elasticsearch installation is easy and highly lightweight when compared to Solr.

* Solr is mostly used for the text search while on the other hand Elasticsearch is mostly used for making analytical querying, grouping, and filtering.

* Elasticsearch gives scope for specifying query analyzer chain while Solr doesn’t have this feature.

* ElasticSearch is a bit simpler in terms of its working and operations, as it includes only a single process as compared to solr.

* Solr is the platform that supports the development of standard search applications, and there is no need for any huge volume of indexing and real-time updates while on the other hand, Elasticsearch achieved the next level with advanced architecture for building real-time search applications.



# Conclusion:


1. Lucene is a Java library that is used for building indexes that are easily searchable and indexable. In simple words, it is a Java search library used to indexing data and search for data in a fast and efficient manner. Various search engines have been built based on this library.

2. Solr and Elasticsearch are complete open-source search engines that are built based on a Java-based search library named Lucene. They are capable to offer search results in a complete user-friendly manner.

3. Solr and ElasticSearch take Lucene APIs, add features on top of them, and make the APIs accessible through an easy to deploy webserver (like tomcat, etc.).



# References


1. [https://www.hcltech.com/blogs/elasticsearch-vs-solr](https://www.hcltech.com/blogs/elasticsearch-vs-solr#:~:text=Solr%20and%20ElasticSearch%20are%20competing,a%20set%20of%20jar%20files.)

2. [https://www.youtube.com/watch](https://www.youtube.com/watch?v=vLEvmZ5eEz0)

3. [http://www.lucenetutorial.com/lucene-vs-solr.html](http://www.lucenetutorial.com/lucene-vs-solr.html#:~:text=A%20simple%20way%20to%20conceptualize,use%20out%2Dof%2Dbox.)

4. [https://medium.com/@shrey](https://medium.com/@shrey_11120/apache-solr-vs-elasticsearch-ae5ab05d8071)

